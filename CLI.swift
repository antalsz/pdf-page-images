import Quartz

////////////////////////////////////////////////////////////////////////////////
// General CLI utilities

struct ArgumentSource {
    let arguments: [String]
    private(set) var index: Int

    init(arguments: [String]) {
        self.arguments = arguments
        self.index     = arguments.startIndex
    }
    
    init(arguments: [String], index: Int) {
        self.arguments = arguments
        self.index     = index < arguments.startIndex
          ? arguments.endIndex
          : index;
    }

    func peek() -> String? {
        guard index < arguments.endIndex else { return nil }
        return arguments[index]
    }
    
    mutating func next() -> String? {
        return next(as: { $0 }, if: { _ in true })
    }

    mutating func next(if pred: (String) -> Bool) -> String? {
        return next(as: { $0 }, if: pred)
    }

    mutating func next<T>(as conv: (String) -> T?) -> T? {
        return next(as: conv, if: { _ in true })
    }

    mutating func next<T>(as conv: (String) -> T?, if pred: (T) -> Bool) -> T? {
        if let result = peek().flatMap(conv), pred(result) {
            index += 1
            return result
        } else {
            return nil
        }
    }

    mutating func rest() -> ArraySlice<String> {
        let result = arguments[index...]
        index = arguments.endIndex
        return result
    }

    mutating func nextCGFloat(
      if pred: (CGFloat) -> Bool = { _ in true }
    ) -> CGFloat? {
        return next(as: { Double($0).map { CGFloat($0) } }, if: pred)
    }
}

struct Option<Argument> {
    let description: String
    let metavar:     String?
    let parser:      (inout ArgumentSource) -> Argument?
}

struct PositionalArgument<Argument> {
    let description: String
    let epithet:     String
    let metavar:     String
    let required:    Bool
    let parser:      (String) -> Argument?
}

struct OptionParser<Argument> : ExpressibleByDictionaryLiteral {
    let options: [([String], Option<Argument>)]
    let byName:  [String: Option<Argument>]
    
    var isEmpty: Bool { return options.isEmpty }

    init(dictionaryLiteral: ([String], Option<Argument>)...) {
        self.init(options: dictionaryLiteral)!
    }
    
    init?<S: Sequence>(options: S)
      where S.Element == ([String], Option<Argument>)
    {
        var byName: [String: Option<Argument>] = [:]
        for (names, option) in options {
            for name in names {
                guard byName[name] == nil, name != "--" else {
                    return nil
                }
                byName[name] = option
            }
        }
        
        self.options = Array(options)
        self.byName  = byName
    }

    func option(_ name: String) -> Option<Argument>? {
        return byName[name]
    }

    func parse<Result>(
      arguments:     inout ArgumentSource,
      with:          (Argument)           throws -> Result?,
      unknownOption: (String)             throws -> Result,
      badArgument:   (String)             throws -> Result,
      rest:          (ArraySlice<String>) throws -> Result
    ) rethrows -> Result {
        while let name = arguments.next(if: { $0.hasPrefix("-") }) {
            if name == "--" {
                break
            }
            
            guard let opt = option(name) else {
                return try unknownOption(name)
            }
            
            guard let arg = opt.parser(&arguments) else {
                return try badArgument(name)
            }

            if let abort = try with(arg) {
                return abort
            }
        }
        
        return try rest(arguments.rest())
    }

    func parse<Early>(
      arguments:     inout ArgumentSource,
      with:          (Argument) throws -> Early?,
      unknownOption: (String)   throws -> Early,
      badArgument:   (String)   throws -> Early
    ) rethrows -> Either<Early, ArraySlice<String>> {
        return try parse(arguments:     &arguments,
                         with:          { try with($0).map { .left($0) } },
                         unknownOption: { .left(try unknownOption($0)) },
                         badArgument:   { .left(try badArgument($0)) },
                         rest:          { .right($0) })
    }

    func parse<Early>(
      arguments:     inout ArgumentSource,
      unknownOption: (String) throws -> Early,
      badArgument:   (String) throws -> Early
    ) rethrows -> Either<Early, ([Argument], ArraySlice<String>)> {
        var results: [Argument] = []
        return try parse(arguments:     &arguments,
                         with:          { results.append($0); return nil },
                         unknownOption: { .left(try unknownOption($0)) },
                         badArgument:   { .left(try badArgument($0)) },
                         rest:          { .right((results, $0)) })
    }

    func helpItems() -> [(String, String)] {
        return options.map {
            (OptionParser.formatOption($0, separator: ","), $0.1.description)
        }
    }

    func usages() -> [String] {
        return options.map {
            "[" + OptionParser.formatOption($0, separator: "|") + "]"
        }
    }

    static func formatOption(_ option: ([String], Option<Argument>),
                             separator: String) -> String {
        let (names, opt) = option
        return (
          names.sorted { $0.count < $1.count }.joined(separator: separator)
            + (opt.metavar.map { " " + $0 } ?? "")
        )
    }
}

struct PositionalArgumentParser<Argument> : ExpressibleByArrayLiteral {
    let positional:        [PositionalArgument<Argument>]
    let flexibleOptionals: Bool
    let required:          Int
    let optional:          Int

    var isEmpty: Bool { return positional.isEmpty }
    
    init(positional: [PositionalArgument<Argument>],
         flexibleOptionals: Bool = false) {
        var required: Int = 0
        var optional: Int = 0

        for pos in positional {
            if pos.required {
                required += 1
            } else {
                optional += 1
            }
        }

        self.positional        = positional
        self.flexibleOptionals = flexibleOptionals
        self.required          = required
        self.optional          = optional
    }

    init(arrayLiteral: PositionalArgument<Argument>...) {
        self.init(positional: arrayLiteral)
    }

    func parse<Coll: Collection, Result>(
      arguments:
        Coll,
      with:
        (Argument) throws -> Result?,
      missingArguments:
        (ArraySlice<PositionalArgument<Argument>>) throws -> Result,
      extraArguments:
        (Coll.SubSequence) throws -> Result,
      badArgument:
        (PositionalArgument<Argument>, String) throws -> Result,
      success:
        () -> Result
    ) rethrows -> Result where Coll.Element == String {
        let argCount = arguments.count
        guard argCount >= required else {
            return try missingArguments(
              positional.filter { $0.required }.dropFirst(argCount))
        }

        guard argCount <= required + optional else {
            return try extraArguments(arguments.dropFirst(required+optional))
        }

        var neededOptionals = argCount - required
        var ix = arguments.startIndex
        for pos in positional {
            let arg = arguments[ix]
            guard pos.required || neededOptionals > 0  else {
                continue
            }
            
            guard let val = pos.parser(arg) else {
                if pos.required || !flexibleOptionals {
                    return try badArgument(pos, arg)
                } else {
                    continue
                }
            }
            if let abort = try with(val) {
                return abort
            }
            if !pos.required { neededOptionals -= 1 }
            ix = arguments.index(after: ix)
            if ix >= arguments.endIndex { break }
        }

        guard ix >= arguments.endIndex else {
            return try extraArguments(arguments[ix...])
        }
        
        return success()
    }

    func parse<Coll: Collection, Early>(
      arguments:
        Coll,
      missingArguments:
        (ArraySlice<PositionalArgument<Argument>>) throws -> Early,
      extraArguments:
        (Coll.SubSequence) throws -> Early,
      badArgument:
        (PositionalArgument<Argument>, String) throws -> Early
    ) rethrows -> Either<Early,[Argument]> where Coll.Element == String {
        var result: [Argument] = []
        return try parse(arguments:        arguments,
                         with:             { result.append($0); return nil },
                         missingArguments: { .left(try missingArguments($0)) },
                         extraArguments:   { .left(try extraArguments($0)) },
                         badArgument:      { .left(try badArgument($0, $1)) },
                         success:          { .right(result) })
    }

    func helpItems() -> [(String, String)] {
        return positional.map { ($0.metavar, $0.description) }
    }

    func usages() -> [String] {
        return positional.map { $0.required ? $0.metavar : "[\($0.metavar)]" }
    }
}

struct CLIProgram<OptArg,PosArg> {
    let defaultName: String
    let description: String
    let options:     OptionParser<OptArg>
    let positional:  PositionalArgumentParser<PosArg>

    func parse<Result>(
      arguments:
        inout ArgumentSource,
      option:
        (OptArg) throws -> Result?,
      positional:
        (PosArg) throws -> Result?,
      success:
        () -> Result,
      unknownOption:
        (String) throws -> Result,
      badOptionArgument:
        (String) throws -> Result,
      missingPositionalArguments:
        (ArraySlice<PositionalArgument<PosArg>>) throws -> Result,
      extraPositionalArguments:
        (ArraySlice<String>) throws -> Result,
      badPositionalArgument:
        (PositionalArgument<PosArg>, String) throws -> Result
    ) rethrows -> Result {
        return try self.options.parse(
          arguments:     &arguments,
          with:          option,
          unknownOption: unknownOption,
          badArgument:   badOptionArgument
        ) { rest in
            try self.positional.parse(
              arguments:        rest,
              with:             positional,
              missingArguments: missingPositionalArguments,
              extraArguments:   extraPositionalArguments,
              badArgument:      badPositionalArgument,
              success:          success
            )
        }
    }

    func parse<Early>(
      arguments:
        inout ArgumentSource,
      unknownOption:
        (String) throws -> Early,
      badOptionArgument:
        (String) throws -> Early,
      missingPositionalArguments:
        (ArraySlice<PositionalArgument<PosArg>>) throws -> Early,
      extraPositionalArguments:
        (ArraySlice<String>) throws -> Early,
      badPositionalArgument:
        (PositionalArgument<PosArg>, String) throws -> Early
    ) rethrows -> Either<Early, ([OptArg], [PosArg])> {
        var optionResults:     [OptArg] = []
        var positionalResults: [PosArg] = []
        
        return try parse(
          arguments:
            &arguments,
          option: {
              optionResults.append($0)
              return nil
          },
          positional: {
              positionalResults.append($0);
              return nil
          },
          success: {
              .right((optionResults, positionalResults))
          },
          unknownOption: {
              .left(try unknownOption($0))
          },
          badOptionArgument: {
              .left(try badOptionArgument($0))
          },
          missingPositionalArguments: {
              .left(try missingPositionalArguments($0))
          },
          extraPositionalArguments: {
              .left(try extraPositionalArguments($0))
          },
          badPositionalArgument: {
              .left(try badPositionalArgument($0, $1))
          }
        )
    }
    
    func printUsage(name: String, columns: Int = defaultColumns) -> () {
        let preamble = "Usage: \(name) "
        print(preamble, terminator: "")
        printWrapped(
          words:       options.usages() + positional.usages(),
          columns:     columns,
          indent:      preamble.count,
          indentFirst: false
        )
    }

    func printHelp(name:       String,
                   columns:    Int    = defaultColumns,
                   separation: Int    = defaultSeparation,
                   indent:     Int    = defaultIndent) -> () {
        printUsage(name: name, columns: columns)
        printWrapped(
          description.replacingOccurrences(of: "${name}", with: name),
          columns: columns,
          indent:  indent
        )
        
        if !(options.isEmpty && positional.isEmpty) {
            print()
            print("Arguments:")
            printAlignedList(
              options.helpItems() + positional.helpItems(),
              columns:    columns,
              separation: separation,
              indent:     indent
            )
        }
    }

    func printUsage(columns: Int = defaultColumns) -> () {
        printUsage(name: defaultName, columns: columns)
    }

    func printHelp(columns:    Int = defaultColumns,
                   separation: Int = defaultSeparation,
                   indent:     Int = defaultIndent) -> () {
        printHelp(name:       defaultName,
                  columns:    columns,
                  separation: separation,
                  indent:     indent)
    }

    // Can't use `static let` in generic types
    static var defaultColumns:    Int { return 80 }
    static var defaultSeparation: Int { return 2  }
    static var defaultIndent:     Int { return 2  }
}

extension CLIProgram where PosArg == () {
    init(
      defaultName: String,
      description: String,
      options:     OptionParser<OptArg>
    ) {
        self.init(defaultName: defaultName,
                  description: description,
                  options:     options,
                  positional:  [])
    }
}

extension CLIProgram where OptArg == () {
    init(
      defaultName: String,
      description: String,
      positional:  PositionalArgumentParser<PosArg>
    ) {
        self.init(defaultName: defaultName,
                  description: description,
                  options:     [:],
                  positional:  positional)
    }
}

extension CLIProgram where OptArg == (), PosArg == () {
    init(
      defaultName: String,
      description: String
    ) {
        self.init(defaultName: defaultName,
                  description: description,
                  options:     [:],
                  positional:  [])
    }
}
