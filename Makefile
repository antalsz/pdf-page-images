pdf-page-images: $(wildcard *.swift)
	swiftc -o $@ $^

clean:
	rm -f pdf-page-images

.PHONY: clean
