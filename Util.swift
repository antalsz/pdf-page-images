////////////////////////////////////////////////////////////////////////////////
// Utilities

extension Optional {
    func fold<T>(none nfn: @autoclosure () throws -> T,
                 some sfn: (Wrapped) throws -> T
    ) rethrows -> T {
        switch self {
        case .none:        return try nfn()
        case .some(let x): return try sfn(x)
        }
    }
    
    func getOrThrow(_ error: @autoclosure () -> Error) throws -> Wrapped {
        switch self {
        case .some(let x): return x
        case .none:        throw error()
        }
    }
}

enum Either<L,R> {
    case left(L)
    case right(R)

    func fold<T>(left: (L) throws -> T, right: (R) throws -> T) rethrows -> T {
        switch self {
        case .left(let l):  return try left(l)
        case .right(let r): return try right(r)
        }
    }
    
    func bimap<L2,R2>(
      left  lfn: (L) throws -> L2,
      right rfn: (R) throws -> R2
    ) rethrows -> Either<L2,R2> {
        switch self {
        case .left(let l):  return .left(try lfn(l))
        case .right(let r): return .right(try rfn(r))
        }
    }
}

extension Collection {
    func joined<Str: StringProtocol>(
      show:        (Element) -> Str,
      separator:   String,
      conjunction: String,
      empty:       String = ""
    ) -> String {
        switch count {
        case 0: return empty
        case 1: return String(show(first!))
        case 2:
            let ix1 = startIndex
            let ix2 = index(after: ix1)
            return show(self[ix1]) + " " + conjunction + " " + show(self[ix2])
        default:
            let pre  = dropLast().map { show($0) + separator + " " }.joined()
            let post = suffix(1).map(show).joined()
            return pre + conjunction + " " + post
        }
    }
}

extension Collection where Element: StringProtocol {
    func joined(separator:   String,
                conjunction: String,
                empty: String = "") -> String {
        return joined(show:        { $0 },
                      separator:   separator,
                      conjunction: conjunction,
                      empty:       empty)
   }
}

func printWrapped(
  _ string:    String,
  columns:     Int,
  indent:      Int  = 0,
  indentFirst: Bool = true
) -> () {
    printWrapped(
      paragraphs:
        string.split(separator: "\n",
                     omittingEmptySubsequences: false).map {p -> [Substring] in
            let indentation = p.prefix { $0 == " " }
            let words       = p.split(separator: " ")
            return (indentation.isEmpty ? [] : [indentation]) + words
        },
      columns:     columns,
      indent:      indent,
      indentFirst: indentFirst
    )
}

func printWrapped<Seq: Sequence>(
  paragraphs:  Seq,
  columns:     Int,
  indent:      Int  = 0,
  indentFirst: Bool = true
) -> () where Seq.Element: Sequence, Seq.Element.Element: StringProtocol {
    let indentStr = String(repeating: " ", count: indent)

    var sep      = indentFirst ? indentStr : ""
    var sepCount = indent
    for paragraph in paragraphs {
        var column = 0
        for word in paragraph {
            let wordCount = word.count

            if column + sepCount + wordCount > columns {
                print("\n", indentStr, word,
                      separator: "", terminator: "")
                column  = indent + wordCount
            } else {
                print(sep, word,
                      separator: "", terminator: "")
                column += sepCount + wordCount
            }

            if [".", "?", "!"].contains(where: word.hasSuffix) {
                sep      = "  "
                sepCount = 2
            } else {
                sep      = " "
                sepCount = 1
            }
        }
        print()

        sep      = indentStr
        sepCount = indent
    }
}

func printWrapped<Seq: Sequence>(
  words:       Seq,
  columns:     Int,
  indent:      Int  = 0,
  indentFirst: Bool = true
) -> () where Seq.Element: StringProtocol {
    printWrapped(paragraphs:  [words],
                 columns:     columns,
                 indent:      indent,
                 indentFirst: indentFirst)
}

func printAlignedList<Seq: Sequence>(
  _ definitions: Seq,
  show:          (Seq.Element) -> (String, String),
  columns:       Int,
  separation:    Int = 1,
  indent:        Int = 0
) -> () {
    var lines: [(String,String)] = []
    var prefixWidth = 0
    for def in definitions  {
        let strings = show(def)
        lines.append(strings)
        prefixWidth = max(prefixWidth, strings.0.count)
    }
    
    let lineIndent      = String(repeating: " ", count: indent)
    let columnSeparator = String(repeating: " ", count: separation)
    
    for (prefix, suffix) in lines {
        print(lineIndent,
              prefix.padding(toLength:   prefixWidth,
                             withPad:    " ",
                             startingAt: 0),
              columnSeparator,
              separator:  "",
              terminator: "")

        printWrapped(suffix,
                     columns:     columns,
                     indent:      indent + prefixWidth + separation,
                     indentFirst: false)
    }
}

func printAlignedList<Seq : Sequence>(
  _ definitions: Seq,
  columns:       Int,
  separation:    Int = 1,
  indent:        Int = 0
) -> () where Seq.Element == (String,String) {
    printAlignedList(
      definitions,
      show:       { $0 },
      columns:    columns,
      separation: separation,
      indent:     indent
   )
}
