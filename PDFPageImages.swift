import Quartz
import Darwin

////////////////////////////////////////////////////////////////////////////////
// App-specific CLI

enum CLIError : Error {
    case unknownOption(String)
    case badArgument(for: String)
    case missingArguments(
           ArraySlice<PositionalArgument<PDFConverterPositionalArgument>>)
    case extraArguments(ArraySlice<String>)
    case needOutputOrFormat

    func message() -> String {
        switch self {
        case .unknownOption(let option):
            return "Unknown option \"\(option)\"."
        case .badArgument(for: let option):
            return "Could not parse the argument to \(option)."
        case .missingArguments(let missing):
            if missing.isEmpty {
                return "Missing arguments."
            } else {
                let descriptions = missing.joined(
                  show:        { $0.epithet },
                  separator:   ",",
                  conjunction: "or"
                )
                return "No \(descriptions) specified."
            } 
        case .extraArguments(let args):
            let extra = args.count == 1
              ? "an extra argument"
              : "\(args.count) extra arguments"
            return "Got \(extra): "
              + args.joined(show:      { "\"\($0)\"" },
                          separator:   ",",
                          conjunction: "and",
                          empty:       "(none)")
              + "."
        case .needOutputOrFormat:
            return """
              Cannot determine the target image format; you must provide \
              either an image format (with --format/-f) or an output filename.
              """
        }
    }
}
    
enum PDFConverterOption {
    case dpi(CGFloat)
    case quality(CGFloat)
    case format(ImageFormat)
    case quiet(Bool)
    case background(CGColor?)
    case rawType(CFString)
    // Other destination properties and CGImageProperties would have to be added
    // manually, because they're nested dictionaries and the value types aren't
    // consistent.  See <https://developer.apple.com/documentation/
    // imageio/1464962-cgimagedestinationaddimage> for more information.
    
    case help
    case listFormats
}

enum PDFConverterPositionalArgument {
    case input(String)
    case output(String)
}

enum PDFConverterMode {
    case convert(converter: PDFConverter, paths: PDFConverter.Paths)
    case help
    case listFormats
    case argumentError(CLIError)
}

struct PDFConverterApp {
    let name: String
    let mode: PDFConverterMode

    static let program: CLIProgram<PDFConverterOption,
                                   PDFConverterPositionalArgument> = CLIProgram(
        defaultName: "pdf-page-images",
        
        description: """
          Convert a PDF file to images of its pages.  The output filenames are \
          given as a template.  If the string "%d" is present, then it is \
          replaced with the page number, and "%%" is replaced with "%"; \
          otherwise (even if another "%" is present but not followed by "d"), \
          the page number is inserted after a dash right before the \
          extension.  If the template is omitted, then the default template is \
          the input but with its extension replaced by the appropriate image \
          extension.  Page numbers always have enough leading zeros so that \
          every page number is of equal length.

          Examples, assuming "input.pdf" is a 10-page PDF:

              % ${name} --format png input.pdf
              input-01.png
              input-02.png
              ...
              input-10.png

              % ${name} input.pdf output.jpg
              output-01.jpg
              output-02.jpg
              ...
              output-10.jpg

              % ${name} input.pdf output_pg%d.tiff
              output_pg01.tiff
              output_pg02.tiff
              ...
              output_pg10.tiff
          """,
        
        options: [
          ["--dpi", "-d"]: Option(
            description: """
              The DPI of the generated images.  A positive real number.  \
              (Default: 300)
              """,
            metavar: "FLOAT",
            parser:  { args in args.nextCGFloat { 0 < $0 }.map { .dpi($0) } }
          ),
          
          ["--quality", "-q"]: Option(
            description: """
              The compression quality of the generated images, if relevant.  A \
              real number between 0 and 1, inclusive.  (Default: 1)
              """,
            metavar: "FLOAT",
            parser:  { args in
                args.nextCGFloat { 0 <= $0 && $0 <= 1 }.map { .quality($0) }
            }
          ),
          
          ["--format", "-f"]: Option(
            description: """
              The image format to generate.  Supported formats: \
              \(ImageFormat.allCases.joined(show:        { $0.name() },
                                            separator:   ",",
                                            conjunction: "and",
                                            empty:       "none")
              ).  Pass "--list-formats" to see a long list of all possible \
              names for these formats.  (Default: \
              \(ImageFormat.fromExtension.name()))
              """,
            metavar:     "FMT",
            parser:      { args in
                args.next().flatMap(ImageFormat.init).map { .format($0) }
            }
          ),
          
          ["--white", "-w"]: Option(
            description: """
              Interpret a blank background in the PDF as a white background.  \
              (Default)
              """,
            metavar:     nil,
            parser:      { _ in .background(CGColor.white) }
          ),
          
          ["--black", "-b"]: Option(
            description: """
              Interpret a blank background in the PDF as a black background.
              """,
            metavar:     nil,
            parser:      { _ in .background(CGColor.black) }
          ),
          
          ["--transparent", "-t"]: Option(
            description: """
              Interpret a blank background in the PDF as transparent, if \
              possible.  For image formats that do not implement transparency, \
              some concrete background color will be used.
              """,
            metavar:     nil,
            parser:      { _ in .background(nil) }
          ),
          
          ["--noisy", "-N"]: Option(
            description: """
              Print the names of the generated image files.  (Default)
              """,
            metavar:     nil,
            parser:      { _ in .quiet(false) }
          ),
          
          ["--quiet", "-Q"]: Option(
            description: """
              Don't print the names of the generated image files.
              """,
            metavar:     nil,
            parser:      { _ in .quiet(true) }
          ),

          ["--raw-type"]: Option(
            description: """
              Specify a raw UTI, such as "public.jpeg", as the image format to \
              generate.
              """,
            metavar: "UTI",
            parser: { args in args.next().map { .rawType($0 as CFString) } }
          ),

          ["--help", "-h"]: Option(
            description: """
              Print this help message and exit.
              """,
            metavar: nil,
            parser: { _ in .help }
          ),

          ["--list-formats"]: Option(
            description: """
              Print out all possible image format names and exit.  These are \
              the possible arguments to the "--format" option; there are \
              multiple ways to refer to many of the image formats.  Image \
              format names are case-insensitive, and \"-\" and \" \" are \
              interchangeable.
              """,
            metavar: nil,
            parser: { _ in .listFormats }
          )
        ],

        positional: [
          PositionalArgument(
            description: "The input PDF file.",
            epithet:     "input PDF file",
            metavar:     "INPUT",
            required:    true,
            parser:      { .input($0) }
          ),

          PositionalArgument(
            description: "The output image filename template.",
            epithet:     "output image filename",
            metavar:     "OUTPUT",
            required:    false,
            parser:      { .output($0) }
          )
        ]
    )

    init() {
        self.init(commandLine: CommandLine.arguments)
    }

    init(commandLine: [String]) {
        let name      = commandLine.first ?? PDFConverterApp.program.defaultName
        var arguments = ArgumentSource(arguments: commandLine, index: 1)
        self.init(name: name, arguments: &arguments)
    }
    
    init(name: String, arguments: inout ArgumentSource) {
        self.name = name
        
        var dpi:        CGFloat   = 300
        var quality:    CGFloat   = 1.0
        var type:       ImageType = .image(format: .fromExtension)
        var background: CGColor?  = CGColor.white
        var quiet:      Bool      = false

        var input:  String? = nil
        var output: String? = nil
        
        self.mode = PDFConverterApp.program.parse(
          arguments:     &arguments,
          
          option:        {
              switch $0 {
              case .dpi(let d):         dpi     = d
              case .quality(let q):     quality = q
              case .format(let fmt):    type    = .image(format: fmt)
              case .background(let bg): background   = bg
              case .quiet(let q):       quiet   = q
                                      
              case .rawType(let uti):   type   = .type(uti: uti)

              case .help:               return .help
              case .listFormats:        return .listFormats
              }
              return nil
          },
          
          positional:    {
              switch $0 {
              case .input(let name):      input  = name
              case .output(let template): output = template
              }
              return nil
          },
          
          success:                    {
            let converter = PDFConverter(dpi:        dpi,
                                         quality:    quality,
                                         type:       type,
                                         background: background,
                                         quiet:      quiet)
            if let paths = converter.pathsFor(input: input!, output: output) {
                return .convert(converter: converter, paths: paths)
            } else {
                return .argumentError(.needOutputOrFormat)
            }
          },
          
          unknownOption:              { .argumentError(.unknownOption($0)) },
          badOptionArgument:          { .argumentError(.badArgument(for: $0)) },
          missingPositionalArguments: { .argumentError(.missingArguments($0)) },
          extraPositionalArguments:   { .argumentError(.extraArguments($0)) },
          badPositionalArgument:      { _, _ in fatalError("""
                                          IMPOSSIBLE: This program does not \
                                          validate its positional arguments.
                                        """) }
        )
    }

    static func printImageFormats() {
        print("Supported image format names:")
        printAlignedList(
          ImageFormat.allCases,
          show: { format in
              ( format.name() + ":",
                ImageFormat.formatNames[format].map {
                    $0.joined(separator: ", ")
                } ?? "(none)" )
          },
          columns: 80,
          indent:  2
        )
    }

    func run() -> Bool {
        do {
            switch self.mode {
            case .convert(let converter, let paths):
                try converter.convert(paths: paths)
                return true
            case .help:
                PDFConverterApp.program.printHelp(name: name)
                return true
            case .listFormats:
                PDFConverterApp.printImageFormats()
                return true
            case .argumentError(let error):
                print(error.message())
                print()
                PDFConverterApp.program.printUsage(name: name)
                return false
            }
        } catch let e as ConversionError {
            print("Error:", e.message())
            return false
        } catch let e {
            print("Error:", e.localizedDescription)
            return false
        }
    }

    func main() -> Never {
        exit(run() ? 0 : 1)
    }
}
