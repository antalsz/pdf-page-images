import Quartz

////////////////////////////////////////////////////////////////////////////////
// High-level interface to PDF conversion

enum ImageFormat : CaseIterable {
    case fromExtension
    case JPEG
    case JPEG2000
    case PNG
    case TIFF
    case GIF
    case BMP

    func name() -> String {
        switch self {
        case .fromExtension: return "from extension"
        case .JPEG:          return "JPEG"
        case .JPEG2000:      return "JPEG 2000"
        case .PNG:           return "PNG"
        case .TIFF:          return "TIFF"
        case .GIF:           return "GIF"
        case .BMP:           return "bitmap"
        }
    }
    
    func fileExtension() -> String {
        switch self {
        case .fromExtension: return ""
        case .JPEG:          return ".jpg"
        case .JPEG2000:      return ".jp2"
        case .PNG:           return ".png"
        case .TIFF:          return ".tiff"
        case .GIF:           return ".gif"
        case .BMP:           return ".bmp"
        }
    }

    func uti() -> CFString? {
        switch self {
        case .fromExtension: return nil
        case .JPEG:          return kUTTypeJPEG
        case .JPEG2000:      return kUTTypeJPEG2000
        case .PNG:           return kUTTypePNG
        case .TIFF:          return kUTTypeTIFF
        case .GIF:           return kUTTypeGIF
        case .BMP:           return kUTTypeBMP
        }
    }
    
    static let formatNames: [ImageFormat: [String]] = [
      .fromExtension: [
        "from extension",
        "extension",
        "from",
        "guess"
      ],

      .JPEG: [
        "jpeg",
        "jpg"
      ],

      .JPEG2000: [
        "jpeg 2000",
        "jpeg2000",
        "jp2",
        "j2k",
        "jpeg2k",
        "jpg2k"
      ],

      .PNG: [
        "png"
      ],

      .TIFF: [
        "tiff"
      ],

      .GIF: [
        "gif"
      ],

      .BMP: [
        "bmp",
        "bitmap",
        "windows bitmap"
      ]
    ]

    
    static let namedFormats: [String: ImageFormat] =
      formatNames.reduce(into: [:]) { reversed, info in
          info.value.forEach { name in reversed[name] = info.key }
      }
    
    init?(name: String) {
        let key = name.lowercased().replacingOccurrences(of: "-", with: " ")
        if let format = ImageFormat.namedFormats[key] {
            self = format
        } else {
            return nil
        }
    }
}

enum ImageType {
    case image(format: ImageFormat)
    case type(uti: CFString)

    func uti() -> CFString? {
        switch self {
        case .image(let format): return format.uti()
        case .type(let uti):     return uti
        }
    }

    func fileExtension() -> String? {
        switch self {
        case .image(let format): return format.fileExtension()
        case .type(let uti):     return extensionForUTI(uti).map { ".\($0)" }
        }
    }
}

struct PDFConverter {
    struct Paths {
        let input:        String
        let outputFormat: String

        init?(converter: PDFConverter, input: String, output: String? = nil) {
            if output == nil, case .image(.fromExtension) = converter.type {
                return nil
            }
            
            self.input        = input
            self.outputFormat = output.fold(
              none: PDFConverter.addPageEscape(
                input,
                newExtension: converter.type.fileExtension()
              )
            ) {
                $0.contains("%d") ? $0 : PDFConverter.addPageEscape($0)
            }
        }

        func output(page: Int, width: Int = 0) -> String {
            return self.outputFormat.replacingOccurrences(
              of:   "%d",
              with: String(format: "%0*d", width, page)
            ).replacingOccurrences(
              of:   "%%",
              with: "%"
            )
        }
    }
    
    let dpi:        CGFloat
    let quality:    CGFloat
    let type:       ImageType
    let background: CGColor?
    let quiet:      Bool

    static func addPageEscape(
      _ path: String,
      newExtension: String? = nil
    ) -> String {
        var withPage = path.replacingOccurrences(of: "%", with: "%%")
        let extIx  = withPage.lastIndex(of: ".") ?? withPage.endIndex
        withPage.insert(contentsOf: "-%d", at: extIx)
        if let ext = newExtension {
            let extIx = withPage.lastIndex(of: ".") ?? withPage.endIndex
            withPage.replaceSubrange(extIx..., with: ext)
        }
        return withPage
    }

    func pathsFor(input: String, output: String? = nil) -> Paths? {
        return Paths(converter: self, input: input, output: output)
    }
    
    func convert(paths: Paths) throws -> () {
        let inputURL = URL(fileURLWithPath: paths.input)
        let pdf = try PDFDocument(url: inputURL).getOrThrow(
          ConversionError.couldNotReadPDF(url: inputURL)
        )
        let pageNumberWidth = Int(ceil(log10(Double(abs(pdf.pageCount) + 1))))
          // 0 for 0, but that's OK
        
        try pdf.writePageImages(
          dpi:        dpi,
          background: background,
          type:       type.uti(),
          options:    [kCGImageDestinationLossyCompressionQuality: quality],
          callback:   quiet ? { _,_,_ in } : { output, _, _ in print(output) }
        ) {
            paths.output(page: $0, width: pageNumberWidth)
        }
    }
}
