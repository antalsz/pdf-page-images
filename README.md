# `pdf-page-images` - Convert a PDF into images, one per page

This application uses macOS’s [CoreGraphics] API to produce higher-quality
results than I was able to get using [ImageMagick]; if you know how to do as
well or better with more cross-platform tools, please let me know!

[CoreGraphics]: https://developer.apple.com/documentation/coregraphics
[ImageMagick]:  https://imagemagick.org/

## Installation

Clone this git repository, and then build the executable with `make`.  The
generated executable is named `pdf-page-images`; copy it to a directory on your
`PATH` to install it.

## Documentation

Convert a PDF file to images of its pages.  The output filenames are given as a
template.  If the string `%d` is present, then it is replaced with the page
number, and `%%` is replaced with `%`; otherwise (even if another `%` is present
but not followed by `d`), the page number is inserted after a dash right before
the extension.  If the template is omitted, then the default template is the
input but with its extension replaced by the appropriate image extension.  Page
numbers always have enough leading zeros so that every page number is of equal
length.

Examples, assuming `input.pdf` is a 10-page PDF:

```
% ./pdf-page-images --format png input.pdf
input-01.png
input-02.png
...
input-10.png
```

```
% ./pdf-page-images input.pdf output.jpg
output-01.jpg
output-02.jpg
...
output-10.jpg
```

```
% ./pdf-page-images input.pdf output_pg%d.tiff
output_pg01.tiff
output_pg02.tiff
...
output_pg10.tiff
```

## Usage

    pdf-page-images [-d|--dpi FLOAT] [-q|--quality FLOAT]
                    [-f|--format FMT]
                    [-w|--white] [-b|--black] [-t|--transparent]
                    [-N|--noisy] [-Q|--quiet]
                    [--raw-type UTI]
                    [-h|--help] [--list-formats]
                    INPUT [OUTPUT]
### Arguments

* `-d,--dpi FLOAT`: The DPI of the generated images.  A positive real
  number.  *(Default: 300)*

* `-q,--quality FLOAT`: The compression quality of the generated images, if
  relevant.  A real number between 0 and 1, inclusive. *(Default: 1)*

* `-f,--format FMT`: The image format to generate.  Supported formats: from
  extension, JPEG, JPEG 2000, PNG, TIFF, GIF, and bitmap. Pass `--list-formats`
  to see a long list of all possible names for these formats, or see below.
  *(Default: from extension)*

* `-w,--white`: Interpret a blank background in the PDF as a white background.
  *(Default)*

* `-b,--black`: Interpret a blank background in the PDF as a black background.

* `-t,--transparent`: Interpret a blank background in the PDF as transparent, if
  possible.  For image formats that do not implement transparency, some concrete
  background color will be used.

* `-N,--noisy`: Print the names of the generated image files.  *(Default)*

* `-Q,--quiet`: Don't print the names of the generated image files.

* `--raw-type UTI`: Specify a raw UTI, such as `public.jpeg`, as the image
  format to generate.

* `-h,--help`: Print a help message and exit.

* `--list-formats`: Print out all possible image format names and exit.  These
  are the possible arguments to the `--format` option; there are multiple ways
  to refer to many of the image formats. Image format names are
  case-insensitive, and `-` and space are interchangeable.

* `INPUT`: The input PDF file.

* `OUTPUT`: The output image filename template.

### Supported image format names

* From extension: `from extension`, `extension`, `from`, `guess`
* JPEG: `jpeg`, `jpg`
* JPEG 2000: `jpeg 2000`, `jpeg2000`, `jp2`, `j2k`, `jpeg2k`, `jpg2k`
* PNG: `png`
* TIFF: `tiff`
* GIF: `gif`
* Bitmap: `bmp`, `bitmap`, `windows bitmap`
