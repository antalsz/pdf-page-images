import Quartz

////////////////////////////////////////////////////////////////////////////////
// Convert PDFs

extension CGContext {
    static let adobeRGB1998Space: CGColorSpace =
      CGColorSpace(name: CGColorSpace.adobeRGB1998)!

    static func adobe(width: Int, height: Int) throws -> CGContext {
        return try CGContext(
          data:             nil, // Auto-allocate
          width:            width,
          height:           height,
          bitsPerComponent: 8,
          bytesPerRow:      0, // Auto-compute
          space:            adobeRGB1998Space,
          bitmapInfo:       CGImageAlphaInfo.premultipliedLast.rawValue
                              // I'm really not sure what the right value for
                              // the alpha channel setting is.  This produced
                              // better results before I was using a white
                              // background, although indistinguishable from
                              // `premultipliedFirst`.  I think PNGs use RGBA,
                              // so I went with `premultipliedLast`, but
                              // ¯\_(ツ)_/¯.
        ).getOrThrow(
          ConversionError.couldNotCreateContext(width: width, height: height)
        )
    }
}

extension CGImage {
    func write(to url: URL, dpi: CGFloat? = nil,
               type: CFString? = nil,
               options extraOptions: [CFString: Any] = [:]
    ) throws -> () {
        guard let uti = type ?? url.extensionTypeIdentifier() else {
            throw ConversionError.unknownFileType(url: url)
        }
        guard UTTypeConformsTo(uti, kUTTypeImage) else {
            throw ConversionError.nonImageType(
              url: url, deduced: type == nil, type: uti)
        }
        let dest = try CGImageDestinationCreateWithURL(
          url as CFURL, uti, 1, nil
        ).getOrThrow(
          ConversionError.couldNotCreateDestination(
            url: url, type: type, dpi: dpi, options: extraOptions)
        )

        var options: [CFString: Any]
        switch dpi {
        case .some(let dpi):
            options = [kCGImagePropertyDPIWidth:  dpi,
                       kCGImagePropertyDPIHeight: dpi]
            options.merge(extraOptions) { (computed, _) in computed }
        case .none:
            options = extraOptions
        }
        
        CGImageDestinationAddImage(dest, self, options as CFDictionary)
        guard CGImageDestinationFinalize(dest) else {
            throw ConversionError.couldNotWriteImage(
              url: url, type: type, dpi: dpi, options: extraOptions
            )
        }
    }
}

extension PDFPage {
    static let standardDPI: CGFloat = 72
    
    func toImage(dpi:             CGFloat,
                 background:      CGColor?      = nil,
                 for boundingBox: PDFDisplayBox = .cropBox
    ) throws -> CGImage {
        let scale  = dpi/PDFPage.standardDPI
        let bounds = self.bounds(for: boundingBox)

        let ctx = try CGContext.adobe(width:  Int(scale*bounds.width),
                                      height: Int(scale*bounds.height))

        // Draw it!
        transform(ctx, for: boundingBox)
        ctx.scaleBy(x: scale, y: scale)
        if let bgcolor = background {
            ctx.setFillColor(bgcolor)
            ctx.fill(CGRect(x: 0, y: 0, width: ctx.width, height: ctx.height))
        }
        draw(with: boundingBox, to: ctx)

        return try ctx.makeImage().getOrThrow(
          ConversionError.couldNotConvert(page: self, dpi:  dpi)
        )
    }
}

extension PDFDocument {
    func forPageImages(dpi:             CGFloat,
                       background:      CGColor?      = nil,
                       for boundingBox: PDFDisplayBox = .cropBox,
                       with:            (Int, CGImage) throws -> ()
    ) throws -> () {
        for ix in 0..<pageCount {
            try with(ix+1, page(at: ix)!.toImage(dpi:        dpi,
                                                 background: background,
                                                 for:        boundingBox))
        }
    }

    func writePageImages(
      dpi:             CGFloat,
      background:      CGColor?      = nil,
      for boundingBox: PDFDisplayBox = .cropBox,
      type:            CFString? = nil,
      options:         [CFString: Any],
      callback:        (String, Int, CGImage) throws -> () = { _,_,_ in },
      file:            (Int) -> String
    ) throws -> () {
        try forPageImages(dpi:        dpi,
                          background: background,
                          for:        boundingBox) { page, image in
            let destination = file(page)
            try callback(destination, page, image)
            try image.write(to:      URL(fileURLWithPath: destination),
                            dpi:     dpi,
                            type:    type,
                            options: options)
        }
    }
}
