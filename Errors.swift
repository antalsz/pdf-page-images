import Quartz

////////////////////////////////////////////////////////////////////////////////
// Conversion errors

enum ConversionError : Error {
    case couldNotReadPDF(url: URL)
    case couldNotCreateContext(width: Int, height: Int)
    case couldNotConvert(page: PDFPage, dpi: CGFloat)
    case unknownFileType(url: URL)
    case nonImageType(url: URL, deduced: Bool, type: CFString)
    case couldNotCreateDestination(url: URL, type: CFString?, dpi: CGFloat?,
                                   options: [CFString: Any])
    case couldNotWriteImage(url: URL, type: CFString?, dpi: CGFloat?,
                            options: [CFString: Any])

    static func prettyURL(_ url: URL) -> String {
        return url.isFileURL ? url.relativePath : url.absoluteString
    }
    
    static func destinationMessage(_ intro: String,
                                   url: URL, type: CFString?, dpi: CGFloat?,
                                   options: [CFString: Any]) -> String {
        let urlDesc     = ConversionError.prettyURL(url)
        let typeDesc    = type.map {
            " with the requested image type \"\($0)\""
        } ?? ""
        let dpiDesc     = dpi.map { "DPI: \($0)" }
        let optionsDesc = options.isEmpty
          ? Optional<String>.none
          : "\(dpi == nil ? "" : "other ")options: \(options.description)"
        let infoDescs   = [dpiDesc, optionsDesc].compactMap { $0 }
        let infoDesc    = infoDescs.isEmpty
          ? ""
          : " (\(infoDescs.joined(separator: "; ")))"
        return "\(intro) \(urlDesc)\(typeDesc)\(infoDesc)."
    }
    
    func message() -> String {
        switch self {
        case .couldNotReadPDF(let url):
            return """
              Could not read a PDF from "\(ConversionError.prettyURL(url))".
              """
        case .couldNotCreateContext(let width, let height):
            return """
              Could not create a graphics context for a \(width)×\(height) \
              image.
              """
        case .couldNotConvert(let page, let dpi):
            let pageNumber = page.pageRef.map { "page \($0.pageNumber)" }
              ?? "unknown page"
            let label      = page.label.map { " (label \"\($0)\")" } ?? ""
            let document   = page.document.flatMap {
                $0.documentURL
            }.map(ConversionError.prettyURL) ?? "an input PDF"
            return """
              Could not convert \(pageNumber)\(label) of \(document) to an \
              image at \(dpi) DPI.
              """
        case .unknownFileType(let url):
            return """
              Unknown file type for \(ConversionError.prettyURL(url)) based on \
              its extension "\(url.pathExtension)."
              """
        case .nonImageType(let url, let deduced, let type):
            let what = deduced
              ? "The deduced type of \(ConversionError.prettyURL(url)) " +
                "(based on its extension \"\(url.pathExtension)\")"
              : "The requested file type \(type)"
            return "\(what) is not an image type."
        case .couldNotCreateDestination(let url, let type, let dpi,
                                        let options):
            return ConversionError.destinationMessage(
              "Could not create the image destination at",
              url: url, type: type, dpi: dpi, options: options)
        case .couldNotWriteImage(let url, let type, let dpi, let options):
            return ConversionError.destinationMessage(
              "Could not write the converted image to",
              url: url, type: type, dpi: dpi, options: options)
        }
    }
}
