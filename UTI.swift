import Foundation

////////////////////////////////////////////////////////////////////////////////
// UTIs

func utiForExtension(_ ext: String) -> CFString? {
    return UTTypeCreatePreferredIdentifierForTag(
      kUTTagClassFilenameExtension, ext as CFString, nil
    ).map { $0.takeRetainedValue() }.flatMap {
        CFStringHasPrefix($0, "dyn." as CFString) ? nil : $0
    }
}

func extensionForUTI(_ uti: CFString) -> String? {
    return UTTypeCopyPreferredTagWithClass(
      uti, kUTTagClassFilenameExtension
    )?.takeRetainedValue() as String?
}

extension URL {
    func extensionTypeIdentifier() -> CFString? {
        return utiForExtension(self.pathExtension)
    }
}
